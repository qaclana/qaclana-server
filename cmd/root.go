// Copyright © 2017 The Qaclana Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"gitlab.com/qaclana/qaclana-server/pkg/server"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "qaclana-server",
	Short: "The Server component in Qaclana",
	Long: `This is the server component for Qaclana, the component to which
the individual firewall instances will connect to.`,
	Run: func(cmd *cobra.Command, args []string) {
		server.Start()
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.qaclana-server.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	RootCmd.Flags().IntP("port", "p", 8000, "The port to bind the main server to")
	RootCmd.Flags().IntP("healthcheck-port", "", 9000, "The port to bind the healthcheck server to")
	RootCmd.Flags().IntP("grpc-port", "", 10000, "The port to bind the gRPC interface to")
	viper.BindPFlag("port", RootCmd.Flags().Lookup("port"))
	viper.BindPFlag("healthcheck-port", RootCmd.Flags().Lookup("healthcheck-port"))
	viper.BindPFlag("grpc-port", RootCmd.Flags().Lookup("grpc-port"))

	RootCmd.Flags().StringP("backend-hostname", "u", "qaclana-backend.qaclana-infra.svc.cluster.local", "The hostname to the Qaclana Backend")
	RootCmd.Flags().IntP("backend-grpc-port", "", 10000, "The gRPC port for the Qaclana Backend")
	viper.BindPFlag("backend-hostname", RootCmd.Flags().Lookup("backend-hostname"))
	viper.BindPFlag("backend-grpc-port", RootCmd.Flags().Lookup("backend-grpc-port"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".qaclana-server" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".qaclana-server")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
